from mixer.backend.flask import mixer
from flaskr.models import User, AuthToken, History
from datetime import datetime, timedelta
from flaskr import views
from pytest import fixture
from . import app, db, password


@fixture
def client(app):
    """Define a client fixture that will allow us to run our views without having a
    running server."""

    yield app.test_client()


def test_health_check(client):
    """Test that we are able to access the health-check url."""

    response = client.get("/")
    data = response.json

    assert "message" in data
    assert "System online." == data["message"]


def test_register(client, db):
    """Test that the view for creating a new account works."""

    # test successfully creating a user
    response = client.post(
        "register", json={"username": "username", "password": "password"}
    )
    assert response.status_code == 200

    # test trying to create a new user with the same username
    response = client.post(
        "register", json={"username": "username", "password": "other_password"}
    )
    assert response.status_code == 400

    # test trying to create a new user without sending arguments
    response = client.post("register", json={})
    assert response.status_code == 400
    response = client.post("register")
    assert response.status_code == 400

    # test trying to create a new user with a password that is too short
    response = client.post(
        "register", json={"username": "other_user", "password": "asdf"}
    )
    assert response.status_code == 400

    # test trying to create a new user without a username or without a password
    response = client.post("register", json={"username": "", "password": "password"})
    assert response.status_code == 400
    response = client.post("register", json={"username": "other_user", "password": ""})
    assert response.status_code == 400


def test_authenticate(client, app, db, password):
    """Test that our authentication process works correctly."""

    with app.app_context():  # when we use the database, we need to be in app context
        user = mixer.blend(User, password=password)
        other_user = mixer.blend(User, password=password)
        token = mixer.blend(
            AuthToken, user=user.id, expiry=datetime.now() + timedelta(hours=1)
        )

        # test successful authentication with existing token
        response = client.post(
            "authenticate", json={"username": user.username, "password": "password"}
        )
        assert response.status_code == 200
        assert response.json["token"] == token.token

        # test successful authentication without existing token
        response = client.post(
            "authenticate",
            json={"username": other_user.username, "password": "password"},
        )
        assert response.status_code == 200

        # test failure when username or password are ignored
        response = client.post("authenticate", json={"username": "username"})
        assert response.status_code == 400
        response = client.post("authenticate", json={"password": "password"})
        assert response.status_code == 400

        # test failure when username or password are blank
        response = client.post(
            "authenticate", json={"username": "", "password": "password"}
        )
        assert response.status_code == 400
        response = client.post(
            "authenticate", json={"username": "username", "password": ""}
        )
        assert response.status_code == 400

        # test failure when user doesn't exist
        response = client.post(
            "authenticate", json={"username": "username", "password": "password"}
        )
        assert response.status_code == 400


def test_reverse(client, app, db, password):
    """Test that our app handles calls to the reverse function correctly."""

    from simple_strop import reverse

    with app.app_context():
        user = mixer.blend(User, password=password)
        token = mixer.blend(AuthToken, user=user.id)
        test_string = "This is a test string"
        compare_string = reverse(test_string)

        # test that an unauthorized request is prevented
        response = client.post("reverse", json={"input": test_string})
        assert response.status_code == 401
        response = client.post(
            "reverse",
            headers={"Authorization": "random_invalid_token"},
            json={"input": test_string},
        )
        assert response.status_code == 401

        # test that correct usage works as intended
        response = client.post(
            "reverse",
            headers={"Authorization": token.token},
            json={"input": test_string},
        )
        assert response.status_code == 200
        assert response.json["output"] == compare_string

        # test failure when lacking input
        response = client.post(
            "reverse",
            headers={"Authorization": token.token},
            json={},
        )
        assert response.status_code == 400


def test_piglatin(client, app, db, password):
    """Test that our app handles calls to the piglatin function correctly."""

    from simple_strop import piglatin

    with app.app_context():
        user = mixer.blend(User, password=password)
        token = mixer.blend(AuthToken, user=user.id)
        test_string = "This is a test string"
        compare_string = piglatin(test_string)

        # test that an unauthorized request is prevented
        response = client.post("piglatin", json={"input": test_string})
        assert response.status_code == 401
        response = client.post(
            "piglatin",
            headers={"Authorization": "random_invalid_token"},
            json={"input": test_string},
        )
        assert response.status_code == 401

        # test that correct usage works as intended
        response = client.post(
            "piglatin",
            headers={"Authorization": token.token},
            json={"input": test_string},
        )
        assert response.status_code == 200
        assert response.json["output"] == compare_string

        # test failure when lacking input
        response = client.post(
            "piglatin",
            headers={"Authorization": token.token},
            json={},
        )
        assert response.status_code == 400


def test_get_history(client, app, db, password):
    """Test that our app handles calls to the get_history function correctly."""

    with app.app_context():
        user = mixer.blend(User, password=password)
        token = mixer.blend(AuthToken, user=user.id)
        records = mixer.cycle(7).blend(History, user=user.id)

        # test that an unauthorized request is prevented
        response = client.get("get-history")
        assert response.status_code == 401
        response = client.get(
            "get-history", headers={"Authorization": "random_invalid_token"}
        )
        assert response.status_code == 401

        # test that correct usage works as intended
        response = client.get("get-history", headers={"Authorization": token.token})
        assert response.status_code == 200
        assert len(response.json["history"]) == 7
